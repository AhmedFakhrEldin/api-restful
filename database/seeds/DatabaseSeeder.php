<?php

use App\Category;
use App\Product;
use App\Transaction;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        //Disable events in seed
        User::flushEventListeners();
        Product::flushEventListeners();

        /*Disable foreign key check condition to do truncate */
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        /*To Empty data in DB*/
        User::truncate();
        Category::truncate();
        Product::truncate();
        Transaction::truncate();
        DB::table('category_product')->truncate();
        /**********************/
        //number data will be create/insert into tables
        $usersQuantity = 1000;
        $categorisQuantity = 30;
        $productsQuantity = 1000;
        $trnsactionsQuantity = 1100;

        factory(User::class,$usersQuantity)->create();
        factory(Category::class,$categorisQuantity)->create();
        factory(Product::class,$productsQuantity)->create()->each(
            function($product){
                $categories = Category::all()->random(mt_rand(1,5))->pluck('id');
                $product->categories()->attach($categories);
            }
        );
        factory(Transaction::class,$trnsactionsQuantity)->create();
    }
}
