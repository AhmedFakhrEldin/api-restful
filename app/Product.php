<?php

namespace App;

use App\Transformers\ProductTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Seller;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property mixed $seller
 * @property mixed $transactions
 * @property \Carbon\Carbon $deleted_at
 * @property mixed $categories
 */
class Product extends Model
{
    //

    use SoftDeletes;

    public $transformer = ProductTransformer::class;
    protected $dates = ['deleted_at'];

    const AVAILABLE_PRODUCT = "available";
    const UNAVAILABLE_PRODUCT = "unavailable";

    protected $fillable =[
        'name',
        'description',
        'quantity',
        'status',
        'image',
        'seller_id'
    ];

    protected $hidden=['pivot'];

    /**
     * @return bool
     */
    public function isAvailable(){
        return $this->status == Product::AVAILABLE_PRODUCT;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function seller(){
        return $this->belongsTo(Seller::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions(){
        return $this->hasMany('App\Transaction');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories(){
        return $this->belongsToMany('App\Category');
    }
}
