<?php

namespace App\Transformers;

use App\Seller;
use League\Fractal\TransformerAbstract;

class SellerTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Seller $seller
     * @return array
     */
    public function transform(Seller $seller)
    {
        return [
            'identifier' => (int) $seller->id,
            'username' => (string) $seller->name,
            'email' => (string) $seller->email,
            'isVerified' => (int) $seller->verified,
            'creationDate' => (string) $seller->created_at,
            'lastChange' => (string) $seller->updated_at,
            'deletedDate' => isset($seller->deleted_at)? (string) $seller->deleted_at:null,
        ];
    }


    /**
     * @param $key
     * @return mixed|null
     */
    public static function originalAttribute($key)
    {
        $attribute = [
            'identifier' => 'id',
            'username' => 'name',
            'email' => 'email',
            'isVerified' => 'verified',
            'creationDate' => 'created_at',
            'lastChange' => 'updated_at',
            'deletedDate' => 'deleted_at',
        ];

        return isset($attribute[$key])? $attribute[$key]:null;
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public static function transformerAttribute($key)
    {
        $attribute = [
            'id'         => 'identifier'  ,
            'name'       => 'username'    ,
            'email'      => 'email'       ,
            'verified'   => 'isVerified'  ,
            'created_at' => 'creationDate',
            'updated_at' => 'lastChange'  ,
            'deleted_at' => 'deletedDate' ,
        ];

        return isset($attribute[$key])? $attribute[$key]:null;
    }
}
