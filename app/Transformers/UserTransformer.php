<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'identifier' => (int) $user->id,
            'username' => (string) $user->name,
            'email' => (string) $user->email,
            'isVerified' => (int) $user->verified,
            'isAdmin' => ($user->admin === 'true') ,
            'creationDate' => (string) $user->created_at,
            'lastChange' => (string) $user->updated_at,
            'deletedDate' => isset($user->deleted_at)? (string) $user->deleted_at:null,

            'links' => [
                [
                    'rel'=>'self',
                    'href' => route('users.show',$user->id),
                ],
            ],
        ];
    }


    /**
     * @param $key
     * @return mixed|null
     */
    public static function originalAttribute($key)
    {
        $attribute = [
            'identifier' => 'id',
            'username' => 'name',
            'email' => 'email',
            'isVerified' => 'verified',
            'isAdmin' => 'admin' ,
            'creationDate' => 'created_at',
            'lastChange' => 'updated_at',
            'deletedDate' => 'deleted_at',
        ];

        return isset($attribute[$key])? $attribute[$key]:null;
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public static function transformerAttribute($key)
    {
        $attribute = [
            'id'         => 'identifier'  ,
            'name'       => 'username'    ,
            'email'      => 'email'       ,
            'verified'   => 'isVerified'  ,
            'admin'      => 'isAdmin'     ,
            'created_at' => 'creationDate',
            'updated_at' => 'lastChange'  ,
            'deleted_at' => 'deletedDate' ,
        ];

        return isset($attribute[$key])? $attribute[$key]:null;
    }
}
