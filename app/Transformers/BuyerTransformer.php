<?php

namespace App\Transformers;

use App\Buyer;
use League\Fractal\TransformerAbstract;

class BuyerTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Buyer $buyer
     * @return array
     */
    public function transform(Buyer $buyer)
    {
        return [
            'identifier' => (int) $buyer->id,
            'username' => (string) $buyer->name,
            'email' => (string) $buyer->email,
            'isVerified' => (int) $buyer->verified,
            'creationDate' => (string) $buyer->created_at,
            'lastChange' => (string) $buyer->updated_at,
            'deletedDate' => isset($buyer->deleted_at)? (string) $buyer->deleted_at:null,
        ];
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public static function originalAttribute($key)
    {
        $attribute = [
            'identifier' => 'id',
            'username' => 'name',
            'email' => 'email',
            'isVerified' => 'verified',
            'creationDate' => 'created_at',
            'lastChange' => 'updated_at',
            'deletedDate' => 'deleted_at',
        ];

        return isset($attribute[$key])? $attribute[$key]:null;
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public static function transformerAttribute($key)
    {
        $attribute = [
          'id'          => 'identifier'  ,
          'name'        => 'username'    ,
          'email'       => 'email'       ,
          'verified'    => 'isVerified'  ,
          'created_at'  => 'creationDate',
          'updated_at'  => 'lastChange'  ,
          'deleted_at'  => 'deletedDate' ,
        ];

        return isset($attribute[$key])? $attribute[$key]:null;
    }

}
