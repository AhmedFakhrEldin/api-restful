<?php

namespace App\Transformers;

use App\Transaction;
use League\Fractal\TransformerAbstract;

class TransactionTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Transaction $transaction
     * @return array
     */
    public function transform(Transaction $transaction)
    {
        return [
            'identifier' => (int) $transaction->id,
            'amount' => (int) $transaction->quantity,
            'buyer' => (int) $transaction->buyer_id,
            'product' => (int) $transaction->product_id,
            'creationDate' => (string) $transaction->created_at,
            'lastChange' => (string) $transaction->updated_at,
            'deletedDate' => isset($transaction->deleted_at)? (string) $transaction->deleted_at:null,

            'links' => [
                [
                    'rel'=>'self',
                    'href' => route('transactions.show',$transaction->id),
                ],
                [
                    'rel'=>'transaction.categories',
                    'href' => route('transactions.categories.index',$transaction->id),
                ],
                [
                    'rel'=>'transaction.sellers',
                    'href' => route('transactions.seller.index',$transaction->id),
                ],
                [
                    'rel'=>'buyer',
                    'href' => route('buyers.show',$transaction->buyer_id),
                ],
                [
                    'rel'=>'product',
                    'href' => route('products.show',$transaction->product_id),
                ],
            ],
        ];
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public static function originalAttribute($key)
    {
        $attribute = [
            'identifier' => 'id',
            'amount' => 'quantity',
            'buyer' => 'buyer_id',
            'product' => 'product_id',
            'creationDate' => 'created_at',
            'lastChange' => 'updated_at',
            'deletedDate' => 'deleted_at',
        ];

        return isset($attribute[$key])? $attribute[$key]:null;
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public static function transformerAttribute($key)
    {
        $attribute = [
            'id'         => 'identifier'  ,
            'quantity'   => 'amount'      ,
            'buyer_id'   => 'buyer'       ,
            'product_id' => 'product'     ,
            'created_at' => 'creationDate',
            'updated_at' => 'lastChange'  ,
            'deleted_at' => 'deletedDate' ,
        ];

        return isset($attribute[$key])? $attribute[$key]:null;
    }
}
