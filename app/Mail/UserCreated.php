<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $userData;

    /**
     * Create a new message instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->userData = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //HTML Emil View
       // return $this->view('view.name');

        //Text Email View
        return $this->text('emails.verify');
    }
}
