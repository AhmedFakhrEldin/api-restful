<?php

namespace App\Http\Controllers\Category;

use App\Category;
use App\Http\Controllers\ApiController;


class CategoryTransactionController extends ApiController
{
    /**
     * CategoryTransactionController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param Category $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Category $category)
    {
        // get only category transaction for products has at less one transaction
        $transaction = $category->products()
            ->whereHas('transactions')
            ->with('transactions')
            ->get()
            ->pluck('transactions')
            ->collapse();
        return $this->showAll($transaction);
    }
}
