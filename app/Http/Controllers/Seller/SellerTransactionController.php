<?php

namespace App\Http\Controllers\Seller;


use App\Http\Controllers\ApiController;
use App\Seller;


class SellerTransactionController extends ApiController
{
    /**
     * SellerTransactionController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param Seller $seller
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Seller $seller)
    {
        $transactions = $seller->products()
            ->whereHas('transactions')
            ->with('transactions')
            ->get()
            ->pluck('transactions')
            ->collapse();
        return $this->showAll($transactions);
    }
}
