<?php

namespace App\Http\Controllers\Buyer;

use App\Buyer;
use App\Http\Controllers\ApiController;


class BuyerProductsController extends ApiController
{
    /**
     * BuyerProductsController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Buyer $buyer
     * @return \Illuminate\Http\JsonResponse
     * @internal param Transaction $transaction
     */
    public function index(Buyer $buyer)
    {
        //Buyer has many transaction & many products in one transaction
        //so this method to get it
        $products = $buyer->transactions()->with('product')
            ->get()
            ->pluck('product');
        return $this->showAll($products);
    }
}
