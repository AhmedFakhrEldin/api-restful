<?php

namespace App\Http\Controllers\Product;

use App\Category;
use App\Http\Controllers\ApiController;
use App\Product;
use Illuminate\Http\Request;

class ProductCategoryController extends ApiController
{
    /**
     * ProductCategoryController constructor.
     */
    public function __construct()
    {
        $this->middleware('client.credentials')->only(['index']);
        $this->middleware('auth:api')->except(['index']);
    }
    /**
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Product $product)
    {
        $buyers = $product->categories;
        return $this->showAll($buyers);
    }

    /**
     * @param Request $request
     * @param Product $product
     * @param Category $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Product $product,Category $category){

        //attach (add new rec so that may make duplicated Data)
        //sync (add new rec with remove old data saved before not perfect when add only new rec)
        //syncWithoutDetaching (add new rec with keep old data and not have any duplicated data)
        $product->categories()->syncWithoutDetaching([$category->id]);
        return $this->showAll($product->categories);
    }

    /**
     * @param Product $product
     * @param Category $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Product $product,Category $category){

        if (!$product->categories()->find($category->id)){
            return $this->errorResponse('The specified category is not a category of this product',404);
        }

        $product->categories()->detach($category->id);
        return $this->showAll($product->categories);
    }

}
