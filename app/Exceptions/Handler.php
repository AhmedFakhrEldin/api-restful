<?php

namespace App\Exceptions;

use App\Traits\ApiResponser;
use Exception;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    use ApiResponser;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // Validations Exception
        if($exception instanceof ValidationException){
            $this->convertValidationExceptionToResponse($exception,$request);
        }
        // Data not found Exception
        if($exception instanceof ModelNotFoundException){
            $modelName = strtolower(class_basename($exception->getModel()));
            return $this->errorResponse("Dose not exists any {$modelName} with the specified identification",
                404);
        }


        if($exception instanceof AuthenticationException){
            return $this->unauthenticated($request,$exception);
        }
        // permissions Exception
        if($exception instanceof AuthorizationException){
            return $this->errorResponse($exception->getMessage(),403);
        }
        // Not Found Pages Exception
        if($exception instanceof NotFoundHttpException){
            return $this->errorResponse('The specified URL cannot be found',404);
        }
        // Method Request Exception
        if($exception instanceof MethodNotAllowedHttpException){
            return $this->errorResponse('The specified method for the request is invalid',405);
        }
        // General Http Exceptions
        if($exception instanceof HttpException){
            return $this->errorResponse($exception->getMessage(),$exception->getStatusCode());
        }

        //Operation Error in DB as condition Or constriction relationship
        if($exception instanceof QueryException){
            $errorCode = $exception->errorInfo[1];
            //$sqlMsg = $exception->errorInfo[2];
            if($errorCode==1451){
                return $this->errorResponse('Cannot remove this resource permanently. It is related with any other resource',409);
            }
            /*else{
                //only for me to view & handel errors
                //return $this->errorResponse('[Code] '.$errorCode.' [Msg] '.$sqlMsg,500);
            }*/

        }

        if($exception instanceof TokenMismatchException){
            return redirect()->back()->withInput(request()->input());
        }

        //dd(config('app.debug'));
        //Check if we in debug mode so view original Exception
        if(config('app.debug')){
            return parent::render($request, $exception);
        }

        return $this->errorResponse('Internal Server Error, Try later',500);
    }

    /**
     * @param ValidationException $e
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {
        $errors = $e->validator->errors()->getMessages();
        if($this->isFronted($request)){
            return $request->ajax() ? response()->json($errors,422) : redirect()
                ->back()->withInput($request->input())->withErrors($errors);
        }
        return $this->errorResponse($errors,422);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param AuthenticationException $exception
     * @return \Illuminate\Http\JsonResponse
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if($this->isFronted($request)){
            return redirect()->guest('login');
        }
        return $this->errorResponse('Unauthenticated.',401);
    }

    //check if it from web or api method
    /**
     * @param $request
     * @return bool
     */
    private function isFronted($request){
        return $request->acceptsHtml() && collect($request->route()->middleware())->contains('web');
    }
}
